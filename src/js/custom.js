/* Theme Name: Worthy - Free Powerful Theme by HtmlCoder
 * Author:HtmlCoder
 * Author URI:http://www.htmlcoder.me
 * Version:1.0.0
 * Created:November 2014
 * License: Creative Commons Attribution 3.0 License (https://creativecommons.org/licenses/by/3.0/)
 * File Description: Place here your custom scripts
 */

$(document).ready(function () {
    //initialize swiper when document ready  
    var mySwiper = new Swiper ('.swiper-container', {
      // Optional parameters
        autoplay: 4000,
		loop: true, 
		pagination: '.swiper-pagination',

	    // Navigation arrows
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	    
	    // And if we need scrollbar
	    scrollbar: '.swiper-scrollbar',
    });
    // lazyload doesn't work for now
    //$("img.lazy").lazyload({
    //	container: $('.swiper-container')
    //});        
  });
