Diaspora Yatra
=======================================================================

This is the campaign website of Diaspora Yatra, an away from keyboard campaign to spread diaspora.

Contribute
=======================================================================
For ease of development and optimization, we are using gulp.

Setting up work environment
--------
* Install [node and npm](http://howtonode.org/introduction-to-npm) if you do not already have it
* `npm install -g gulp`

Cloning repo
--------
You probably need to work on a fork of your own. Otherwise you can request write access to the main repository. Then,

```
git clone git@gitlab.com:piratemovin/diasporayatra.git
cd diasporayatra
npm install
```

gulp dev will open a browser window which will automatically watch for changes you make to the src.

Once you are done developing, you need to push the modifications to the master branch and also deploy the changes to the gh-pages branch

```
git add .
git commit -m "new changes to master"
git push
gulp deploy
```

`gulp deploy` will build the site and then push to the gh-pages branch automatically. Every push will cause a webhook to run which causes yatra.diasporafoundation.org to update automatically.

### FAQ ###
#### gulp deploy fails, gitlab rejects push ####
This seems to be an issue with the cache mechanism of gulp-gh-pages plugin. `rm -rf /tmp/tmpRepo` or restart the computer to fix this.

Credits
=======================================================================

Contributors
---------------
* [Praveen Arimbrathodiyil](https://gitlab.com/u/pravi)
* [Akshay S Dinesh](https://github.com/asdofindia)
* [Balasankar C](https://gitlab.com/u/balasankarc)

Hosting
---------------
Website: [DayScholars.com](https://dayscholars.com/)
Code: [Gitlab](https://gitlab.com/piratemovin/diasporayatra)

Images:
* Each district's images have been taken from wikipedia and are attributed on the website
* [Writing](https://www.flickr.com/photos/jjpacres/3293117576) by  jeffrey james pacres licensed CC-2.0-by-nc-nd and originally uploaded to flickr.com 
* [Ark Arjun](https://poddery.com/people/479a5d2e57813ccf), [Pirate Praveen](https://poddery.com/people/45fa8bea21b8a0f5) and [Hrishi](https://poddery.com/people/296d3a9a21e9e449) for images in [#diasporayatra](https://poddery.com/tags/diasporayatra)

Theme
------------------------------------------------------
- Worthy - http://htmlcoder.me

### Demo images ###
- Unsplash by Crew - http://unsplash.com/

### Fonts ###
- Font Awesome by Dave Gandy - http://fortawesome.github.io/Font-Awesome/
- Google Fonts - http://www.google.com/fonts

### Resources ###
- Bootstrap Framework by @mdo and @fat - http://getbootstrap.com/
- jQuery - https://jquery.org/
- jQuery Appear - by bas2k - https://github.com/bas2k/jquery.appear/
- Modernizr - http://modernizr.com/
- Animate CSS by Daniel T. Eden - http://daneden.github.io/animate.css/
- Isotope Jquery plugin by metafizzy.co - http://isotope.metafizzy.co/
- Backstrech by Scott Robbin - http://srobbin.com/jquery-plugins/backstretch/
- Swiper by iDangero.us - http://www.idangero.us/swiper/ (MIT License)

Plugins
-----
* [rowoot/gulp-gh-pages](https://github.com/rowoot/gulp-gh-pages)
* [colynb/gulp-swig](https://github.com/colynb/gulp-swig/)
* [colynb/gulp-data](https://github.com/colynb/gulp-data)

References and Tools
----------

* [freecontactform.com](http://www.freecontactform.com/email_form.php)
* php.net
* [tomaszbujnowicz/html5-boilerplate-gulp-less](https://github.com/tomaszbujnowicz/html5-boilerplate-gulp-less/)
* [travismaynard.com](http://travismaynard.com/writing/getting-started-with-gulp)
* [css-tricks.com](http://css-tricks.com/snippets/css/prevent-long-urls-from-breaking-out-of-container/)
* [stackoverflow.com](http://stackoverflow.com/questions/6163763/how-to-get-the-at-shell-command-output-in-php)
* [duckduckgo](https://duckduckgo.com) search
* [wikipedia](https://en.wikipedia.org)
* [JsonLint.com](http://jsonlint.com/) for helping out with index.json spaghetti
