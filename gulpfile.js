var fs = require('fs');
var path = require('path');

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')(); // Load all gulp plugins
                                              // automatically and attach
                                              // them to the `plugins` object

var runSequence = require('run-sequence');    // Temporary solution until gulp 4
                                              // https://github.com/gulpjs/gulp/issues/355

var browserSync = require('browser-sync');
var reload      = browserSync.reload;

var deploy = require('gulp-gh-pages');
var request = require('request');

var pkg = require('./package.json');
var dirs = pkg['h5bp-configs'].directories;

var path = require('path');
var data = require('gulp-data');
var swig = require('gulp-swig');
// ---------------------------------------------------------------------
// | Helper tasks                                                      |
// ---------------------------------------------------------------------

gulp.task('clean', function (done) {
    require('del')([
        dirs.dist
    ], done);
});

gulp.task('copy', [
    'html',
    'misc',
    'images'
]);

gulp.task('misc', function () {
    return gulp.src([

        // Copy all files
        dirs.src + '/**/*',
        // Exclude the following files
        // (other tasks will handle the copying of these files)
        '!' + dirs.src + '/js{,/**}',
        '!' + dirs.src + '/less{,/**}',
        '!' + dirs.src + '/css{,/**}',
        '!' + dirs.src + '/plugins{,/**}',
        '!' + dirs.src + '/images{,/**}',
        '!' + dirs.src + '/*.json',
        '!' + dirs.src + '/**/*.html'
    ], {
        // Include hidden files by default
        dot: true
    }).pipe(gulp.dest(dirs.dist))
        .pipe(reload({ stream:true }));
});

gulp.task('images', function () {
    return gulp.src([
        dirs.src + '/images/**/*'
    ], {
        dot: true
    }).pipe(gulp.dest(dirs.dist + '/images'))
        .pipe(reload({ stream:true }));
});

var getJsonData = function(file) {
  //return require('./' + dirs.src +'/' + path.basename(file.path, '.html') + '.json');
  //return require(path.join(path.dirname(file.path), path.basename(file.path, '.html') + '.json'));
  var jsonpath= path.join(path.dirname(file.path), path.basename(file.path, '.html') + '.json');
  if (fs.existsSync(jsonpath)){
    return require(jsonpath);
  }
  else {
    return;
  }
};

var swigoptions = {
  defaults:{
    cache: false,
    autoescape: false
  }
};

gulp.task('html', function() {
    return gulp.src(dirs.src + '/**/*.html')
    .pipe(data(getJsonData))
    .pipe(swig(swigoptions))
    .pipe(gulp.dest(dirs.dist))
    .pipe(reload({ stream:true }));
});

gulp.task('lint:js', function () {
    return gulp.src([
        'gulpfile.js',
        dirs.src + '/js/*.js',
        dirs.test + '/*.js'
    ]).pipe(plugins.jshint())
      .pipe(plugins.jshint.reporter('default'))
      .pipe(plugins.jshint.reporter('jshint-stylish'))
      .pipe(plugins.jshint.reporter('fail'));
});

// Scripts
gulp.task('js', function() {
    return gulp.src([dirs.src + '/js/template.js',
                               dirs.src + '/js/custom.js'])
        .pipe(plugins.concat('main.min.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(dirs.dist + '/js'))
        .pipe(reload({ stream:true }));
});

gulp.task('vendorjs', function() {
    return gulp.src([
            dirs.src + '/plugins/jquery.min.js',
            dirs.src + '/plugins/bootstrap.min.js',
            dirs.src + '/plugins/modernizr.js',
            dirs.src + '/plugins/isotope/isotope.pkgd.min.js',
            dirs.src + '/plugins/jquery.backstretch.min.js',
            dirs.src + '/plugins/jquery.appear.js',
            // dirs.src + '/plugins/jquery.lazyload.min.js',
            dirs.src + '/plugins/swiper.jquery.min.js'])
        .pipe(plugins.concat('plugins.js'))
        .pipe(gulp.dest(dirs.dist + '/js'))
        .pipe(plugins.rename('plugins.min.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(dirs.dist + '/js'))
        .pipe(reload({ stream:true }));
});

// Styles
gulp.task('less', function() {
    return gulp.src(dirs.src + '/less/*.less')
        .pipe(plugins.less())
        .pipe(plugins.autoprefixer())
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(plugins.minifyCss())
        .pipe(gulp.dest(dirs.dist + '/css'))
        .pipe(reload({ stream:true }));
});

gulp.task('css', function() {
    return gulp.src([
            'src/css/bootstrap.css',
            'src/css/font-awesome.css',
            'src/css/animations.css',
            'src/css/style.css',
            'src/css/swiper.min.css',
            'src/css/custom.css'])
        .pipe(plugins.concat('build.css'))
        .pipe(plugins.minifyCss())
        .pipe(gulp.dest(dirs.dist + '/css'))
        .pipe(reload({ stream:true }));
});

// BrowserSync task for starting the server.
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: './' + dirs.dist
        }
    });
});

// Reload all Browsers
gulp.task('bs-reload', function () {
    browserSync.reload();
});

// Deploy
gulp.task('actualdeploy', function() {
  return gulp.src('./dist/**/*')
  .pipe(deploy());
});

// Make the server pull
gulp.task('webhook', function(){
  console.log("requesting server update");
  request('http://yatra.diasporafoundation.org/webhook.php', function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log('server says\n' + body);
    }
    else {
      console.log('server does not respond well');
    }
  });
  return;
});

// ---------------------------------------------------------------------
// | Main tasks                                                        |
// ---------------------------------------------------------------------

// Watch
gulp.task('watch', function () {
    gulp.watch(dirs.src + '/less/**/*.less', ['less']);
    gulp.watch(dirs.src + '/js/**/*.js', ['js']);
    gulp.watch(dirs.src + '/**/*.html', ['html']);
    gulp.watch(dirs.src + '/**/*.json', ['html']);
    gulp.watch(dirs.src + '/images/**/*', ['images']);
    gulp.watch(dirs.src + '/**/*', ['misc']);
});


// Build
gulp.task('build', function (done) {
    runSequence(
        ['clean', 'lint:js'], ['copy', 'css', 'less', 'vendorjs', 'js'],
    done);
});

// Dev
gulp.task('dev', function (done) {
    runSequence(
        'build', ['browser-sync', 'watch'],
    done);
});

// Build and Deploy
gulp.task('deploy', function(done) {
    runSequence(
      'build', 'actualdeploy','webhook',
    done);
});

// Default
gulp.task('default', ['build']);
